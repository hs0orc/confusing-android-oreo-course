package com.example.camsi.eggtimertry;

/**
 * Created by camsi on 2020-05-20.
 */

public class GlobalVars extends MainActivity {

    private static int value2;

    public static int getSeekBarValue() {
        return value2;
    }

    public static void setSeekBarValue(int value) {
        value2 = value;
    }

}