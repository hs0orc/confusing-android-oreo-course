package com.example.camsi.eggtimertry;

import android.os.CountDownTimer;
import android.provider.Settings;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.SeekBar;
import android.widget.TextView;


public class MainActivity extends AppCompatActivity {

   // SeekBar seekBarForTimer;
    TextView timerTextView;
    SeekBar seekBarForTimer;
    Button buttonStop;
    Button buttonGo;
    CountDownTimer cdTimer;
    //CountDownTimer cdTimer;
    Boolean counterIsActive = false;

    public void resetTimer() {
        cdTimer.cancel();
        counterIsActive = true;
    }

   public void stopButton(View view) {
       //Button btnStop = (Button) findViewById(R.id.buttonStop);

       buttonGo.setVisibility( View.VISIBLE );
       buttonStop.setVisibility(View.GONE );


       //onStop();
       //seekBarForTimer.setEnabled(true);
       //seekBarForTimer.setVisibility(View.VISIBLE);

   }

    public void clickButton(View view) {
        //Button btnGo = (Button) findViewById(R.id.buttonGo);
        if (counterIsActive) {

            seekBarForTimer.setProgress(30);
            seekBarForTimer.setEnabled(true);
            cdTimer.cancel();
            counterIsActive = false;

        } else {
            counterIsActive = true;
            seekBarForTimer.setEnabled(false);
            //buttonGo.setVisibility(View.GONE);
            //buttonStop.setVisibility(View.VISIBLE);



            // seekBarForTimer.setVisibility(View.GONE);
            //seekBarForTimer.bringToFront();


            final CountDownTimer cdTimer = new CountDownTimer(seekBarForTimer.getProgress() * 1000, 100) {
                @Override
                public void onTick(long l) {
                    updateTimer((int) (l / 1000));
                    // TextView textTimer = (TextView) findViewById(R.id.textTimer);
                    // textTimer.setText("5000");
                    // TextView textTimer = (TextView) findViewById(R.id.textTimer);
                    //textTimer.setText(hours + ":" + minutes);
                    //TextView textTimer = (TextView) findViewById(R.id.textTimer);
                    //textTimer.setText("remaining" + l);
                    //Log.i("seconds left!", String.valueOf(textTimer));
                }

                @Override
                public void onFinish() {

                    Log.i("We're done", "no more countdown");
                }
            }.start();

            buttonStop.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    buttonGo.setVisibility(View.VISIBLE);
                    buttonStop.setVisibility(View.GONE);
                    seekBarForTimer.setEnabled(true);
                    seekBarForTimer.setProgress(30);
                    seekBarForTimer.setEnabled(true);
                    seekBarForTimer.setVisibility(View.VISIBLE);
                    cdTimer.cancel();
                }
            });

            buttonGo.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    seekBarForTimer.setVisibility(View.GONE);
                    //seekBarForTimer.setVisibility(View.VISIBLE);
                    buttonGo.setVisibility(View.GONE);
                    buttonStop.setVisibility(View.VISIBLE);
                    cdTimer.start();
                }
            });

        }
    }

    public void updateTimer(int secondsLeft) {
        int minutes = secondsLeft / 60;
        int seconds = secondsLeft - (minutes * 60);

        String secondString = Integer.toString(seconds);

        if (secondString.equals("0")) {
            secondString = "00";
        }

        /*int hours = secondsLeft / 4;
        int minutes = (secondsLeft % 4) * 15;*/

        //Log.i("seekbar value", "hours" + hours + "minutes" + minutes);
        timerTextView.setText(Integer.toString(minutes) + ":" + secondString);
       // textTimer = (TextView) findViewById(R.id.textTimer);
        //timerTextView.setText(hours + ":" + minutes);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        buttonStop = (Button) findViewById(R.id.buttonStop);
        buttonGo = (Button) findViewById(R.id.buttonGo);
        //button = (Button) findViewById(R.id.button);
        timerTextView = (TextView) findViewById(R.id.textTimer);
        seekBarForTimer = (SeekBar) findViewById(R.id.seekBarForTimer);
        seekBarForTimer.setMax(24 * 4);
        seekBarForTimer.setProgress(30);
        seekBarForTimer.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int i, boolean b) {
             updateTimer(i);

                /*   int hours = i / 4;
                int minutes = (i % 4) * 15;


                Log.i("seekbar value", "hours" + hours + "minutes" + minutes);

               textTimer = (TextView) findViewById(R.id.textTimer);
                textTimer.setText(hours + ":" + minutes);
*/

                /*GlobalVars ob = new GlobalVars();
                GlobalVars.setSeekBarValue(i);
                int v = GlobalVars.getSeekBarValue();
                Log.i("** doice **", String.valueOf(v));*/


                //TextView textTimer = (TextView) findViewById(R.id.textTimer);
                //textTimer.setText((CharSequence) seekBarForTimer);
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });

    }
}
