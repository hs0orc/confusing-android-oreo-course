package com.example.camsi.listviewdemo;

import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.SeekBar;
import android.widget.Toast;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    ListView myListView;

    public void generateTimesTable(int timesTableNumber) {
        ArrayList<String> timesTableContent = new ArrayList<String>();

        for (int j=1; j <= 10; j++)
        {
            timesTableContent.add(Integer.toString(j * timesTableNumber));
        }
        ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_expandable_list_item_1, timesTableContent);

        myListView.setAdapter(arrayAdapter);

    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        /*myFamily.add("Donk");
        myFamily.add("Fish");
        myFamily.add("Nit");
        myFamily.add("Woof");
*/
        final SeekBar timesSeekBar = (SeekBar) findViewById(R.id.seekbar);

        int[] times = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10};

        final ArrayList<Integer> myFamily = new ArrayList<Integer>();

        for (int s : times) {
            myFamily.add(s);

        }

        final ArrayList<Integer> timesFamily = new ArrayList<Integer>();

        int newValue = 1;
        int[] anArray;
        // int timez = 0;
        for (int x : myFamily) {
            // myFamily[0] == 1, myFamily[1] == 2;
            //int newValue;
            //times[toBeMultiplied] * newValue;
            //  myFamily[toBeMultiplied] * myFamily[toBeMultiplied * 2]; // 1,
            //timez = newValue * toBeMultiplied;
            // int current = x;
            //newValue =
          /*  if (x == 0) {
                continue;
            }*/
            int current = x; // 1
            //boolean even = (current / 2) == 0;
            //boolean odd = (current / 2) != 0;
            if (current % 2 == 0) {
                //timesFamily.add(current * current / 2);
                timesFamily.add(current * current / current + x); // 1 * 1/1 = 1, 2 * 2/2
                //}
            /*if (odd) {
                timesFamily.add(current * current / 3);
            }*/
            }

            // timesFamily.add(current * current/2);



           /* Log.i("INFO #1", toString(newValue));*/
            //newValue = newValue * toBeMultiplied; // 1 * 0, 2 * 1
            //newValue = (toBeMultiplied * newValue); // 0 * 1 ** 1 * 2
            //timesFamily.add(myFamily[x] * myFamily[x * 2]);


            //final ArrayList<Integer> timesFamily = new ArrayList<Integer>();

       /* for (int x : myFamily) {
            newTimes[x] *  newTimes[x] *;
        }
*/
      /*  myFamily.add(0);
        myFamily.add(1);*/

            //Log.i("info **** ", String.valueOf(myFamily));
            int max = 20;
            int startingPosition = 10;
            timesSeekBar.setMax(max);
            timesSeekBar.setProgress(startingPosition);

            generateTimesTable(startingPosition);

            ListView myListView = (ListView) findViewById(R.id.myListView);


            final ArrayAdapter<Integer> arrayAdapter = new ArrayAdapter<Integer>(this, android.R.layout.simple_expandable_list_item_1, timesFamily);

            myListView.setAdapter(arrayAdapter);

            myListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                    //Log.i("Info", timesFamily.get(i).toString());

                    // String familyString = "You selected" +String.valueOf(myFamily.get(i));


                    // Toast.makeText(getApplicationContext(), myFamily.get(i), Toast.LENGTH_SHORT).show();

                }
            });

            timesSeekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
                @Override
                public void onProgressChanged(SeekBar seekBar, int i, boolean b) {

                    int min = 1;
                    int timesTableNumber;

                    if (i < min) {
                        timesTableNumber = min;
                        timesSeekBar.setProgress(min);
                    } else {
                        timesTableNumber = i;
                    }

                    Log.i("seekbar value", Integer.toString(timesTableNumber));
                    generateTimesTable(timesTableNumber);


                    // int[] times = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10};
                   // Log.i("info", Integer.toString(i));
                //timesSeekBar.setEnabled(true);
                //timesFamily.add(timesSeekBar.getProgress());
               //  int pos = 0;
               // timesFamily.get(arrayAdapter.pos) = "1000";
                    //timesFamily.set(i.toString());
                    arrayAdapter.notifyDataSetChanged();
                    //seekBar.getContext(timesFamily.get(i));

                    //timesFamily.set(i);
               /*for (int time : times) {
                   Log.i("scrub control", time[i]);
               }*/


                }

                @Override
                public void onStartTrackingTouch(SeekBar seekBar) {

                }

                @Override
                public void onStopTrackingTouch(SeekBar seekBar) {

                }
            });


            // final ListView myListView = (ListView) findViewById(R.id.myListView);


        /*final ArrayList<String> myFamily = new ArrayList<String>();

        myFamily.add("Donk");
        myFamily.add("Fish");
        myFamily.add("Nit");
        myFamily.add("Woof");*/

        /*ArrayAdapter<Integer> arrayAdapter = new ArrayAdapter<Integer>(this, android.R.layout.simple_expandable_list_item_1, myFamily);

        myListView.setAdapter(arrayAdapter);

        myListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Log.i("Info", myFamily.get(i));

               // String familyString = "You selected" +String.valueOf(myFamily.get(i));


                Toast.makeText(getApplicationContext(), myFamily.get(i), Toast.LENGTH_SHORT).show();

            }
        });*/


        }
    }
}
