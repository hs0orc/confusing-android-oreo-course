package com.example.camsi.animations;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;

public class MainActivity extends AppCompatActivity {

    boolean bartIsShowing = true;



    public void fade(View view) {


     /*   ImageView bartImageView = (ImageView) findViewById(R.id.bartImageView);
        ImageView homerImageView = (ImageView) findViewById(R.id.homerImageView);
             bartImageView.animate().translationX(750).setDuration(1);*/


        Log.i("Info", "Imageview tapped");

        ImageView bartImageView = (ImageView) findViewById(R.id.bartImageView);

        ImageView homerImageView = (ImageView) findViewById(R.id.homerImageView);

        if (bartIsShowing) {

            bartIsShowing = false;

            Log.i("Info", "** 1st bart is showing then false **");

            bartImageView.animate().alpha(0).setDuration(2000);

            homerImageView.animate().alpha(1).setDuration(2000);

        } else {

            bartIsShowing = true;
            Log.i("Info", "** 2nd bart is not showing then true **");
            bartImageView.animate().alpha(1).setDuration(2000);

            homerImageView.animate().alpha(0).setDuration(2000);

        }

    }



        @Override
        protected void onCreate (Bundle savedInstanceState){
            super.onCreate(savedInstanceState);
            setContentView(R.layout.activity_main);
        }
    }

